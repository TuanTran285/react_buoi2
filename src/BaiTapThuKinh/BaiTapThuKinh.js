import React, { Component } from 'react'
import glassList from './dataGlasses.json'
import GlassesList from './GlassesList'
import Model from './Model'

export default class BaiTapThuKinh extends Component {
    state = {
        glasses: {
            "id": 1,
            "price": 30,
            "name": "GUCCI G8850U",
            "url": "./glassesImage/v1.png",
            "img": "./glassesImage/g1.jpg",
            "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
        },
    }
    xemChiTiet = (newGlasses) => {
        this.setState({
            glasses : newGlasses
        })
    }
  render() {
    return (
      <div className='container'>
        <Model newGlasses = {this.state.glasses}/>
        <GlassesList glassesList={glassList} xemChiTiet={this.xemChiTiet}/>
      </div>
    )
  }
}
