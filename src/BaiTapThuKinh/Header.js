import React, { Component } from 'react'

export default class Header extends Component {
  render() {
    return (
      <div style={{backgroundColor: "#3f3f3f", height: 60}}>
        <h4 style={{textTransform: 'uppercase', color: "white", textAlign: "center", lineHeight: "60px"}}>Try glasses app online</h4>
      </div>
    )
  }
}
