import React, { Component } from 'react'

export default class Glasses extends Component {
    render() {
      const {glasses, index, xemChiTiet} = this.props
    return (
        <div className='col-2' key={index}>
            <img src={glasses.img} onClick={() => xemChiTiet(glasses)} style={{width: 120, height: 120, textAlign: "center"}} alt="" />
        </div>
      
    )
  }
}
