import React, { Component } from 'react'
import Glasses from './Glasses'

export default class GlassesList extends Component {
    render() {
      const {glassesList, xemChiTiet} =  this.props
    return (
      <div className='row mt-5 Larger shadow bg-white'>
            {glassesList.map((glasses, index) => {
                return (
                    <Glasses glasses={glasses} key={index} xemChiTiet={xemChiTiet}/>
                )
            })}
      </div>
    )
  }
}
