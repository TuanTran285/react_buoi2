import React, { Component } from "react";

export default class Model extends Component {
  render() {
    const {newGlasses} = this.props
    return (
        <div className="row mt-5">
          <div className="col-6 text-center relative">
            <img
              src="./glassesImage/model.jpg"
              style={{ with: 300, height: 300 }}
              alt=""
            />
            <img src={newGlasses.url} style={{width: 140, height: 50, position:"absolute", left:"38%", top: "25%"}} alt="" />
            <div style={{position: "absolute", color: "white", textAlign: "left", padding: 5, backgroundColor: "#f8ab77", width: 250, bottom: 0, left: "28%"}}>
                <h3 style={{fontSize: 16}}>Tên: {newGlasses.name}</h3>
                <p style={{margin: 0}}>Giá: {newGlasses.price}$</p>
            </div>
          </div>
          <div className="col-6 text-center">
            <img
              src="./glassesImage/model.jpg"
              style={{ with: 300, height: 300 }}
              alt=""
            />
          </div>
        </div>
    );
  }
}
