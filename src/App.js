import BaiTapThuKinh from "./BaiTapThuKinh/BaiTapThuKinh";
import Header from "./BaiTapThuKinh/Header";

function App() {
  return (
    <div className="App">
      <Header/>
      <BaiTapThuKinh/>
    </div>
  );
}

export default App;
